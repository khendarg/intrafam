#!/usr/bin/env python

import intrafam
import argparse
from Bio import SeqIO
import subprocess
from kevtools_common.types import TCID
import os

def get_longest_contig(arr, threshold=0.7):
	spans = []

	for i, x in enumerate(arr):
		if x >= threshold:
			if len(spans) == 0: spans.append([i, i+1])
			elif spans[-1][1] == i: spans[-1][1] += 1

	if len(spans) > 0:
		lengths = [span[1] - span[0] for span in spans]
		length, span = sorted(zip(lengths, spans))[-1]
		return [span]
	else: return [[0,0]]


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('--cutoff', type=float, default=0.7, help='Occupancy threshold (default: 0.7)')
	parser.add_argument('-e', '--evalue', default=1e-3, type=float, help='E-value cutoff (default: 1e-3)')
	parser.add_argument('--discontiguous', action='store_true', help='Also bring in discontiguous alignments instead of just using the longest contiguous span')
	parser.add_argument('--scorefunction', default='fixed', help='Score function to apply (default: fixed)')

	parser.add_argument('infile', nargs='?', default='/dev/stdin', help='BLAST table to open (default: stdin)')
	parser.add_argument('--tcdb', help='Location of tcdb.faa, a sanitized all-TCDB FASTA')

	args = parser.parse_args()

	scorefunction = intrafam.ScoreFunction.get_method(args.scorefunction)

	if args.tcdb is None:
		tcfn = 'tcdb.faa'
		if not os.path.isfile('tcdb.faa'): 
			subprocess.call(['extractFamily.pl', '-i', 'all', '-o', '.'])
	else: tcfn = args.tcdb

	seqlist = SeqIO.parse(tcfn, 'fasta')
	seqdict = {}
	for seq in seqlist:
		seqdict[TCID(seq.name.replace('..', '.'))] = seq.seq

	with open(args.infile) as fh:
		alignments = intrafam.import_alignments(fh, expect_cutoff=args.evalue)

		for fam in sorted(alignments):
			goodspans = intrafam.calculate_goodspans(alignments[fam], scorefunction=scorefunction)

			normalized = {}

			for k in goodspans:
				normalized[k] = goodspans[k] / max(goodspans[k])

			print('>{} ({} members)'.format(fam, len(goodspans)))

			for tcid in goodspans:
				spans = get_longest_contig(normalized[tcid], threshold=args.cutoff)
				seq = ''
				if tcid not in seqdict:
					continue
					#raise KeyError properly
				for span in spans: 
					seq += seqdict[tcid][span[0]:span[1]]
				print('>>{}'.format(tcid))
				print('{}\n'.format(intrafam.numerate(seq, add_ladder=False)))
