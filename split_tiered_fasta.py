#!/usr/bin/env python

import argparse
import os

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', nargs='?', default='/dev/stdin')
	parser.add_argument('-o', '--outdir', required=True)

	args = parser.parse_args()

	if not os.path.isdir(args.outdir): os.mkdir(args.outdir)

	with open(args.infile) as fh:
		currentfamily = ''
		outf = None
		for l in fh:
			if l.startswith('>>'):
				outf.write(l[1:])
				
			elif l.startswith('>'):
				if outf is not None: outf.close()
				currentfamily = l[1:].split()[0]
				outf = open('{}/{}.faa'.format(args.outdir, currentfamily), 'w')
			
			elif l.strip():
				outf.write(l)
